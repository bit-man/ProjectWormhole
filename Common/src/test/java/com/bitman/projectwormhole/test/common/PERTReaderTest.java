package com.bitman.projectwormhole.test.common;

import com.bitman.projectwormhole.common.IDurationReader;
import com.bitman.projectwormhole.common.PERTReader;
import com.bitman.projectwormhole.common.ParsingException;
import com.bitman.projectwormhole.common.TaskDuration;
import org.junit.Test;

import static org.junit.Assert.*;

import java.io.File;
import java.util.Map;

/**
 * Description : TOOL DESCRIPTION HERE !!!
 * Date: 11/23/13
 * Time: 9:58 AM
 */
public class PERTReaderTest {
    @Test
    public void test() throws ParsingException, Exception {

        // ToDo : read file from resource
        File duration = new File(new File(".").getAbsoluteFile(), "src/test/resources/durations.csv");
        IDurationReader reader = new PERTReader(duration);
        reader.parse();
        Map<Integer,TaskDuration> taskMap = ((PERTReader) reader).getTaskMap();

        int id1 = 1;
        TaskDuration t1 = new TaskDuration(id1,5,7,4);
        assertEquals(t1, taskMap.get(id1)) ;

        int id2 = 2;
        TaskDuration t2 = new TaskDuration(id2,6,8,5);
        assertEquals(t2, taskMap.get(id2)) ;

    }
}
