package com.bitman.projectwormhole.common;

import java.util.List;

/**
 * Description : TOOL DESCRIPTION HERE !!!
 * Date: 11/23/13
 * Time: 9:55 AM
 */
public interface IBasedReader<T> {

    // ToDo make it return a Map<Integer,T> as com.bitman.projectwormhole.common.PERTReader.getTaskMap()
    public List<T> getTasks();

    public void parse() throws Exception, ParsingException;
}
