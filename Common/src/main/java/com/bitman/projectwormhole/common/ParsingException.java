package com.bitman.projectwormhole.common;

/**
 * Description : Parsing Exception
 * Date: 11/19/13
 * Time: 6:21 AM
 */
public class ParsingException extends Throwable {
    public static final Integer END_OF_TASK = 1;

    private final int causeNum;

    public ParsingException(String msg, int cause) {
        super(msg);
        this.causeNum = cause;
    }

    public int getCauseNum() {
        return causeNum;
    }
}
