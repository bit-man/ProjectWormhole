package com.bitman.projectwormhole.common;

/**
 * Description : Task Duration
 * Date: 11/23/13
 * Time: 9:49 AM
 */
public class TaskDuration {
    private Integer id;
    private Integer expected;
    private Integer worst;
    private Integer best;


    public TaskDuration(Integer id, Integer expected, Integer worst, Integer best) {
        this.id = id;
        this.expected = expected;
        this.worst = worst;
        this.best = best;
    }

    public Integer getId() {
        return id;
    }

    public Integer getExpected() {
        return expected;
    }

    public Integer getWorst() {
        return worst;
    }

    public Integer getBest() {
        return best;
    }


    public boolean equals(Object that) {
        if (that instanceof TaskDuration) {
            TaskDuration thatTask = (TaskDuration) that;
            return (this.getId().equals(thatTask.getId()) &&
                    this.getExpected().equals(thatTask.getExpected()) &&
                    this.getBest().equals(thatTask.getBest()) &&
                    this.getWorst().equals(thatTask.getWorst())
            );
        } else {
            return super.equals(that);
        }
    }

    @Override
    public String toString() {
        return "TaskDuration{" +
                "id=" + id +
                ", expected=" + expected +
                ", worst=" + worst +
                ", best=" + best +
                '}';
    }
}
