package com.bitman.projectwormhole.common;

import java.util.List;

/**
 * Description : Reader interface
 * Date: 11/18/13
 * Time: 7:01 PM
 */
public interface IReader
        extends IBasedReader<Task> {
}
