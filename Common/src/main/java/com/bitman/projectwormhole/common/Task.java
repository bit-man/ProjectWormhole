package com.bitman.projectwormhole.common;

import java.util.List;

/**
 * Description : Task
 * Date: 11/17/13
 * Time: 4:36 PM
 */
public class Task {

    private String uid;
    private  String name ;
    private String duration;
    private String durationFormat ;
    private List<Predecessor> predecessors;
    private Integer outlineLevel;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDurationFormat() {
        return durationFormat;
    }

    public void setDurationFormat(String durationFormat) {
        this.durationFormat = durationFormat;
    }

    public void setPredecessors(List<Predecessor> predecessors) {
        this.predecessors = predecessors;
    }

    public List<Predecessor> getPredecessors() {
        return predecessors;
    }

    public void setOutlineLevel(Integer outlineLevel) {
        this.outlineLevel = outlineLevel;
    }

    public Integer getOutlineLevel() {
        return outlineLevel;
    }
}
