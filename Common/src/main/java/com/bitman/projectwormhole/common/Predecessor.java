package com.bitman.projectwormhole.common;

/**
 * Description : TOOL DESCRIPTION HERE !!!
 * Date: 11/19/13
 * Time: 7:05 PM
 */
public class Predecessor {

    private String uid;
    private Integer type;

    public Predecessor(String uid, Integer type) {
        this.uid = uid;
        this.type = type;

    }

    public String getUid() {
        return uid;
    }


    public Integer getType() {
        return type;
    }

    public boolean equals(Object that) {
        if (this.getClass().equals(that.getClass())) {
            Predecessor thatP = (Predecessor) that;
            return (this.getUid().equals(thatP.getUid()) && this.getType().equals(thatP.getType()));
        } else
            return false;
    }

}
