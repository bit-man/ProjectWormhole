package com.bitman.projectwormhole.common;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Description : PERT file reader
 * Date: 11/23/13
 * Time: 9:44 AM
 */
public class PERTReader
    implements IDurationReader {
    private final File file;
    private List<TaskDuration> taskDurations;
    private Map<Integer,TaskDuration> tasks;

    public PERTReader(File file) {
        this.file = file;
        taskDurations = new ArrayList<TaskDuration>();
        tasks = new HashMap<Integer, TaskDuration>();
    }

    @Override
    public List<TaskDuration> getTasks() {
        return taskDurations;
    }

    public Map<Integer, TaskDuration> getTaskMap() {
        return tasks;
    }

    public void parse() throws IOException {

        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

        String line;
        while ((line = bufferedReader.readLine()) != null) {
            String[] params = line.split(",", 4);

            Integer id = Integer.parseInt(params[0]);
            TaskDuration t = new TaskDuration(id,Integer.parseInt(params[1]), Integer.parseInt(params[3]), Integer.parseInt(params[2]));
            taskDurations.add(t);
            tasks.put(id, t);
        }
    }
}
