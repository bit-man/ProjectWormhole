package com.bitman.projectwormhole.writer.mcsimulation.xml;

import com.bitman.projectwormhole.common.Task;
import mcsimulations.Tools;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Description : Monte Carlo simluation writer
 * Date: 11/18/13
 * Time: 7:00 PM
 */
public class Writer {

    private final File file;
    private List<Task> tasks;

    private List<Integer> activityId;
    private List<String> activityDesc;
    private List<String> activityDistribution;
    private List<List<Integer>> activityDistributionParam;
    private List<String> activityPrecedence;

    public void setActivityId(List<Integer> activityId) {
        this.activityId = activityId;
    }

    public void setActivityDesc(List<String> activityDesc) {
        this.activityDesc = activityDesc;
    }

    public void setActivityDistribution(List<String> activityDistribution) {
        this.activityDistribution = activityDistribution;
    }

    public void setActivityDistributionParam(List<List<Integer>> activityDistributionParam) {
        this.activityDistributionParam = activityDistributionParam;
    }

    public void setActivityPrecedence(List<String> activityPrecedence) {
        this.activityPrecedence = activityPrecedence;
    }

    public List<Task> setTasks() {
        return tasks;
    }

    public Writer(File f) {
        this.file = f;
    }

    public void write() throws IOException {
        Tools.saveProject(file, Tools.getDataAct(activityId.size(), activityId, activityDesc, activityPrecedence,
                activityDistribution, activityDistributionParam, null, null, true));
    }

}
