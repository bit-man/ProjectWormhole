package com.bitman.projectwormhole.test.writer.mcsimulation.xml;

import com.bitman.projectwormhole.writer.mcsimulation.xml.Writer;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Description : Monte Carlo simulation test
 * Date: 11/18/13
 * Time: 9:23 PM
 */
public class WriterTest {

    @Test
    public void test() throws IOException {
        final int NUM_ACTIVITIES = 3;

        Writer writer = new Writer(new File("/tmp/writeTest"));
        List<Integer> activityId = new ArrayList<Integer>(NUM_ACTIVITIES);

        activityId.add(1);
        activityId.add(2);
        activityId.add(3);
        writer.setActivityId(activityId);

        List<String> activityDesc = new ArrayList<String>(NUM_ACTIVITIES);
        activityDesc.add("Activity1");
        activityDesc.add("Activity2");
        activityDesc.add("Activity3");
        writer.setActivityDesc(activityDesc);

        List<String> activityDist = new ArrayList<String>(NUM_ACTIVITIES);
        activityDist.add("Gaussian");
        activityDist.add("Gaussian");
        activityDist.add("Gaussian");
        writer.setActivityDistribution(activityDist);

        List<List<Integer>> activityDistParam = new ArrayList<List<Integer>>(NUM_ACTIVITIES);
        List<Integer> paramGaussian = new ArrayList<Integer>(2);
        paramGaussian.add(10); // mean
        paramGaussian.add(1);  // sd
        activityDistParam.add(paramGaussian);
        activityDistParam.add(paramGaussian);
        activityDistParam.add(paramGaussian);
        writer.setActivityDistributionParam(activityDistParam);

        List<String> activityPrecedence = new ArrayList<String>(NUM_ACTIVITIES);
        activityPrecedence.add("");
        activityPrecedence.add("1");
        activityPrecedence.add("1,2");
        writer.setActivityPrecedence(activityPrecedence);

        writer.write();
    }


}
