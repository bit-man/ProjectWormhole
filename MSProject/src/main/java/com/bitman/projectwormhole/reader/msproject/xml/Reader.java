package com.bitman.projectwormhole.reader.msproject.xml;

import com.bitman.projectwormhole.common.*;


import static org.junit.Assert.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Description : MS Project XML Format Reader
 * Date: 11/17/13
 * Time: 3:46 PM
 */

public class Reader
implements IReader {

    private static final char TAG_BEGIN = '<';
    private static final boolean JUMP_INSIDE_TASK = true;
    private final File file;
    private boolean endOfStream;
    private List<Task> tasks;

    public List<Task> getTasks() {
        return tasks;
    }

    public Reader(File f) {
        this.file = f;
        endOfStream = false;
    }

    public void parse() throws Exception, ParsingException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

        try {
            jumpTo("<Tasks>", bufferedReader, ! JUMP_INSIDE_TASK);
            tasks = new ArrayList<Task>();

            while (true) {
                jumpTo("<Task>", bufferedReader, ! JUMP_INSIDE_TASK);
                Task t = new Task();
                t.setUid(jumpToAndGet("<UID>", bufferedReader));
                t.setName(jumpToAndGet("<Name>", bufferedReader));
                t.setOutlineLevel(Integer.valueOf(jumpToAndGet("<OutlineLevel>", bufferedReader)));
                t.setDuration(jumpToAndGet("<Duration>", bufferedReader));

                String durationFormat = jumpToAndGet("<DurationFormat>", bufferedReader);
                assertEquals( 7, Integer.parseInt(durationFormat) );
                t.setDurationFormat(durationFormat);

                t.setPredecessors(getPredecessors(bufferedReader));

                // Add task only if no exceptions thrown
                tasks.add(t);
            }
        } catch (ParserEndOfFileException e) {
            // Nothing to see here !
        } catch (ParsingException e) {
            System.out.println("Error : " + e.getMessage());
            e.printStackTrace();
            throw e;
        } finally {
            bufferedReader.close();
        }

    }

    private List<Predecessor> getPredecessors(BufferedReader bufferedReader) throws Exception, ParsingException, ParserEndOfFileException {
        List<Predecessor> ret = new ArrayList<Predecessor>();
        try {
            jumpTo("<PredecessorLink>", bufferedReader, JUMP_INSIDE_TASK);

            // Iterates up to end of current task
            while(true) {
                Predecessor predecessor = new Predecessor(jumpToAndGet("<PredecessorUID>", bufferedReader),
                                                          Integer.parseInt(jumpToAndGet("<Type>", bufferedReader)));
                ret.add(predecessor);
            }

        } catch (ParsingException e){
            if (e.getCauseNum() != ParsingException.END_OF_TASK) {
                 throw e;
             }
        }
        return ret;  //To change body of created methods use File | Settings | File Templates.
    }

    private String jumpToAndGet(String s, BufferedReader bufferedReader) throws Exception, ParserEndOfFileException, ParsingException {
        String line = jumpTo(s, bufferedReader, JUMP_INSIDE_TASK);

        if (endOfStream)
            throw new ParserEndOfFileException();

        int valueStart = line.indexOf('>', line.indexOf(TAG_BEGIN)) + 1;
        int valueEnd = line.indexOf(TAG_BEGIN, valueStart);
        return line.substring(valueStart, valueEnd).trim();
    }


    private String jumpTo(String s, BufferedReader bufferedReader, boolean jumpInsideTask) throws IOException, ParsingException {
        String line;
        while ((line = bufferedReader.readLine()) != null && !line.contains(s))
            if (jumpInsideTask && line.contains("</Task>"))
                throw new ParsingException("End of task reached while jumping to " + s, ParsingException.END_OF_TASK);
        endOfStream = line == null;
        return line;
    }
}
