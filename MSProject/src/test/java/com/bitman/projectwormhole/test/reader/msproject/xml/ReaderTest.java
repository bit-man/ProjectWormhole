package com.bitman.projectwormhole.test.reader.msproject.xml;

import com.bitman.projectwormhole.common.ParsingException;
import com.bitman.projectwormhole.common.Predecessor;
import com.bitman.projectwormhole.common.Task;
import com.bitman.projectwormhole.reader.msproject.xml.Reader;
import org.junit.Test;
import static org.junit.Assert.*;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Description : TOOL DESCRIPTION HERE !!!
 * Date: 11/17/13
 * Time: 4:25 PM
 */
public class ReaderTest {
    @Test
    public void test() throws Exception, ParsingException {
        // ToDo : read file from resource
        File tareas = new File( new File(".").getAbsoluteFile(),"src/test/resources/Tareas.xml");
        Reader reader = new Reader(tareas);
        reader.parse();

        assertEquals(29, reader.getTasks().size());
        assertTask(reader.getTasks().get(0), "1", "PT80H0M0S", "Análisis y Diseño Paneles", new ArrayList<Predecessor>(), 1);

        List<Predecessor> pred15 = new ArrayList<Predecessor>();
        pred15.add(new Predecessor("20",1));
        assertTask(reader.getTasks().get(14), "15", "PT40H0M0S", "interfaz PC", pred15, 2);

        List<Predecessor> pred17 = new ArrayList<Predecessor>();
        pred17.add(new Predecessor("19",1));
        pred17.add(new Predecessor("18",1));
        assertTask(reader.getTasks().get(16), "17", "PT40H0M0S", "interfaz gráfica", pred17, 2);
    }

    private void assertTask(Task task, String uid, String duration, String name, List<Predecessor> predecessors, Integer outlineLevel) {
        assertEquals(uid, task.getUid());
        assertEquals(duration, task.getDuration());
        assertEquals("7", task.getDurationFormat());
        assertEquals(name, task.getName());
        assertEquals(predecessors, task.getPredecessors());
        assertEquals(outlineLevel, task.getOutlineLevel());
    }
}
