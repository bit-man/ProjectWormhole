package com.bitman.projectwormhole.convert;

import com.bitman.projectwormhole.common.*;
import com.bitman.projectwormhole.reader.msproject.xml.Reader;
import com.bitman.projectwormhole.writer.mcsimulation.xml.Writer;
import mcsimulations.Distribution;

import static org.junit.Assert.*;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;
import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Description : Conversion tool
 * Date: 11/18/13
 * Time: 9:47 PM
 */
public class Main {

    private static final Integer FINISH_TO_START = new Integer(1);
    private static final String WARNING = "Warning:";
    private static final String ERROR = "ERROR ";
    private static final int HOURS_PER_WORK_DAY = 8;
    private static List<String> errMsg = new ArrayList<String>();

    public static void main(String[] arg) throws Exception, ParsingException {
        System.out.println("Project Wormhole is up and running");

        File MSProjectFile = new File(arg[0]);
        File MonteCarloFile = new File(arg[1]);
        File paramsFile = new File(arg[2]);

        IReader readMSP = new Reader(MSProjectFile);
        readMSP.parse();
        List<Task> tasks = readMSP.getTasks();

        IDurationReader rDuration = new PERTReader(paramsFile);
        rDuration.parse();
        Map<Integer,TaskDuration> tasksDuration = ((PERTReader) rDuration).getTaskMap();

        Writer writerMC = new Writer(MonteCarloFile);
        writerMC.setActivityDistributionParam(readActivityDist(tasksDuration, tasks));
        writerMC.setActivityDistribution(getDistribution(tasks.size()));
        writerMC.setActivityDesc(getActivitiesDescription(tasks));
        writerMC.setActivityPrecedence(getActivitiesPrecedence(tasks));
        writerMC.setActivityId(getActivitiesId(tasks));

        if (errMsg.size() != 0) {
            printErrMsgs();
            System.exit(-1);
        }


        writerMC.write();

        System.out.println("So long, and thanks for all the fish!");
    }

    private static void printErrMsgs() {
        for ( String msg : errMsg)
            System.out.println(msg);
    }

    private static List<Integer> getActivitiesId(List<Task> tasks) {
        List<Integer> ret = new ArrayList<Integer>(tasks.size());

        for ( Task t : tasks)  {
            ret.add(Integer.valueOf(t.getUid()));

            // if OutlineLevel == 1 was decided that they are just to group tasks, not predecessors or successors must be set
            if ( t.getOutlineLevel().equals(1) && ! t.getPredecessors().equals(new ArrayList<Predecessor>()) )
                errMsg.add(ERROR + "task '" + t.getName() + "' (" + t.getUid() +
                        ") has outline level 1 and MUST BE used for grouping purposes only");
        }

        return ret;
    }

    private static List<String> getActivitiesPrecedence(List<Task> tasks) {
        List<String> ret = new ArrayList<String>(tasks.size());
        for ( Task t : tasks)    {
            List<String> predList = new ArrayList<String>();

            // Only finish to start relationship is used
            for ( Predecessor p: t.getPredecessors() )
                if (p.getType().equals(FINISH_TO_START))
                    predList.add(p.getUid());
                else
                    System.out.println(WARNING + "Predecessor " + p.getUid() +
                                       " for task '" + t.getName() + "' (" +t.getUid() + ") is type " + p.getType());

            String p = packAsString(predList);
            System.out.println("Task " + t.getUid() + ", precedent tasks : " + p);
            ret.add(p);
        }
        return ret;
    }

    private static String packAsString(List<String> predecessors) {
        String ret = "";
        Iterator<String> iterator = predecessors.iterator();
        while(iterator.hasNext())
            ret += (iterator.next() + ( iterator.hasNext() ? "," : ""));
        return ret;
    }

    private static List<String> getActivitiesDescription(List<Task> tasks) {
        List<String> ret = new ArrayList<String>(tasks.size());
        for ( Task t : tasks)
            ret.add(t.getName());
        return ret;
    }

    private static List<String> getDistribution(int size) {
        List<String> ret = new ArrayList<String>(size);

        for( int i = 0 ; i < size; i++)
            ret.add("Beta");

        return ret;
    }


    private static List<List<Integer>> readActivityDist(Map<Integer, TaskDuration> taskDuration, List<Task> tasks) throws IOException, DatatypeConfigurationException {
        List<List<Integer>> ret = new ArrayList<List<Integer>>(tasks.size());

        for( Task t : tasks) {
            List<Integer> p = new ArrayList<Integer>(Distribution.BETA.getNumGUIParams());
            ret.add(p);

            DatatypeFactory factory = DatatypeFactory.newInstance();
            Duration duration = factory.newDuration(t.getDuration());

            int uid = Integer.parseInt(t.getUid());
            String task = "Task "+ uid + " : ";
            assertEquals(task + "Days", 0, duration.getDays());
            assertEquals(task + "Minutes", 0, duration.getMinutes());
            assertEquals(task + "Months", 0, duration.getMonths());
            assertEquals(task + "Seconds", 0, duration.getSeconds());
            assertEquals(task + "Years", 0, duration.getYears());

            TaskDuration tDuration = taskDuration.get(uid);
            assertEquals(task + "Duration (in hours),", new Integer(duration.getHours()), tDuration.getExpected());

            // Now set the duration in days, as expected by Monte Carlo Simulations
            p.add(tDuration.getBest() / HOURS_PER_WORK_DAY);
            p.add(tDuration.getWorst()  / HOURS_PER_WORK_DAY);
            p.add(tDuration.getExpected() / HOURS_PER_WORK_DAY);

        }

        return ret;
    }
}
